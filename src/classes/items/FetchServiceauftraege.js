import Items from '.';
import { writeNullToKeys, semiCamelCase } from '../../functions';

class FetchServiceauftraege extends Items {

	constructor(props, signal) {
		super(props, signal);
		this.props = props;
		this.signal = signal;
		this.path = {
			string: props.listingPath,
			id: null
		};
		this.fetch = this.fetch.bind(this);
	}

	fetch = async () => {

		const name = 'Serviceauftraege';
		const camelName = semiCamelCase(name);

		try {
			const json = await super.fetchDbOrServerGet(name, this.path);

			if (!json.path || !json.results || !json.results[camelName]) return [];

			const obj = {
				totalResultCount: json.results.totalResultCount,
				serviceauftraege: writeNullToKeys(json.results.serviceauftraege)
			};

			return obj;

		} catch (error) {
			console.log(6436347);
			console.error(error);
			return error;
		}
	};
}

export default FetchServiceauftraege;
