import Items from '.';
import { getCurrentFilename } from '../../functions';

class FetchGlobaleEinstellung extends Items {

  constructor (props, signal, id) {
    super(props, signal);
    if (!id || isNaN(id)) throw new TypeError(`[${getCurrentFilename(__filename)}] id nicht vorhanden`);
    this.path = {
      string: `einstellungen/global/${id}`,
      id: id
    };
    this.fetch = this.fetch.bind(this);
  }

  fetch = () => new Promise((resolve, reject) => {
    super.fetchDbOrServerGet('Einstellung', this.path).then(json => {
      if (!json.path || !json.results || !json.results.einstellung){
        reject('[Einstellung.js] !json.path || !json.results || !json.results.einstellung');
        return;
      }
      let einstellung = json.results.einstellung;
      resolve(einstellung);
    }).catch(err => reject(err)); //super.fetchDbOrServerGet('Aktivitaet'
  });
}

export default FetchGlobaleEinstellung;