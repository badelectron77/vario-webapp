import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FetchAdresseEditieren from '../classes/items/FetchAdresseEditieren';

const styles = theme => ({
	container: {
		display: 'flex',
		flexWrap: 'wrap'
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 218
	},
	formControlBeschreibung: {
		margin: theme.spacing(1),
		minWidth: '95%'
	}
});

class AdresseEditierenModal extends React.Component {

	constructor(props) {
		super(props);

		this.controller = new AbortController();
		this._isMounted = false;

		this.state = {};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.updateState = this.updateState.bind(this);
	}

	componentDidMount() {
		this._isMounted = true;
		this.updateState();
	}

	updateState() {
		const { name1, strasse, plz, ort, mobil, telefon1, telefon2, email, webseite, id } = { ...this.props.adresse };
		this.setState({
			name1,
			strasse,
			plz,
			ort,
			mobil,
			telefon1,
			telefon2,
			email,
			webseite,
			id
		});
	}

	componentWillUnmount() {
		this._isMounted = false;
		this.controller.abort();
	}

	handleChange = name => event => {
		this.setState({ [name]: event.target.value });
	};


	componentDidUpdate(prevProps, prevState, snapshot) {
		if (prevProps !== this.props) {
			this.updateState();
		}
	}

	handleSubmit() {
		this.props.closeAdrEditieren();

		const sendData = {
			data: { ...this.state },
			protokollBemerkung: 'Adresse geändert.'
		};

		new FetchAdresseEditieren(this.props.screenprops, this.controller.signal, sendData, this.props.adresse.id).fetch().then(responseOk => {
			if (responseOk) this.props.setTabValueInUrl(0);
			else console.error(786377439);
		});

		this.setState({});
	}

	render() {
		const { classes } = this.props;

		return (
			<div>
				<Dialog
					disableBackdropClick
					disableEscapeKeyDown
					open={this.props.open}
				>
					<DialogTitle>Adresse ändern</DialogTitle>
					<DialogContent>
						<form className={classes.container}>
							<FormControl className={classes.formControl}>
								<TextField
									label="Name 1"
									value={this.state.name1}
									type="text"
									onChange={this.handleChange('name1')}
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Straße"
									value={this.state.strasse}
									type="text"
									onChange={this.handleChange('strasse')}
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Postleitzahl"
									value={this.state.plz}
									onChange={this.handleChange('plz')}
									type="number"
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Ort"
									value={this.state.ort}
									onChange={this.handleChange('ort')}
									type="text"
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Mobil"
									value={this.state.mobil}
									onChange={this.handleChange('mobil')}
									type="text"
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Telefon"
									value={this.state.telefon1}
									onChange={this.handleChange('telefon1')}
									type="text"
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Telefon"
									value={this.state.telefon2}
									onChange={this.handleChange('telefon2')}
									type="text"
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="E-Mail"
									value={this.state.email}
									onChange={this.handleChange('email')}
									type="text"
								/>
							</FormControl>
							<FormControl className={classes.formControl}>
								<TextField
									label="Webseite"
									value={this.state.webseite}
									onChange={this.handleChange('webseite')}
									type="text"
								/>
							</FormControl>
						</form>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.props.closeAdrEditieren} color="primary">
							Abbrechen
            </Button>
						<div style={{ width: '10px' }}></div>
						<Button onClick={this.handleSubmit} color="primary">
							Änderungen speichern
            </Button>
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}

AdresseEditierenModal.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdresseEditierenModal);
