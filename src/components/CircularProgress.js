import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';

class VarioCircularProgress extends Component {

	state = {
		completed: 0
	};

	componentDidMount() {
		this.timer = setInterval(this.progress, 20);
	}

	componentWillUnmount() {
		clearInterval(this.timer);
	}

	progress = () => {
		const { completed } = this.state;
		this.setState({ completed: completed >= 100 ? 0 : completed + 1 });
	};

	render() {

		return (
			<>
				<CircularProgress
					size={this.props.size ? this.props.size : 40}
					thickness={this.props.thickness ? this.props.thickness : 6}
					variant="determinate"
					value={this.state.completed}
				/>
			</>
		);
	}
}

VarioCircularProgress.propTypes = {
	size: PropTypes.number,
	thickness: PropTypes.number
};

export default VarioCircularProgress;