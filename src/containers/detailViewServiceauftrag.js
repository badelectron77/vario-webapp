import React from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TimerIcon from '@material-ui/icons/Timer';

const StyledCard = withStyles({
	root: {
		margin: '8px',
		padding: 8
	}
})(Card);
const left = {
	item: true,
	xs: 6,
	sm: 4
};
const right = {
	item: true,
	xs: 6,
	sm: 8
};

const detailViewServiceauftrag = props => {

	const Kundendaten = () => {
		if (!props.dataItem) return;
		const serviceauftrag = props.dataItem.serviceauftrag.results.serviceauftragDetails;
		const adresse = props.dataItem.adresse;
		// const dash = '—';

		return (
			<>
				<StyledCard>
					<Grid container spacing={1}>
						<Grid xs={12} item container justify="center"><Typography variant="h5" color="secondary">{adresse.anrede}{adresse.name1}</Typography></Grid>
						<Grid xs={12} item container justify="center">
							<Typography variant="subtitle2">
								{serviceauftrag.strasse}, {serviceauftrag.plz} {serviceauftrag.ort}
							</Typography>
						</Grid>
					</Grid>
				</StyledCard>
			</>
		);
	};

	const Assetdaten = () => {
		if (!props.dataItem.asset) return (
			<StyledCard>
				<Typography>Keine Assetdaten vorhanden!</Typography>
			</StyledCard>
		);

		const asset = props.dataItem.asset;
		return (
			<>
				<StyledCard>
					<Grid container spacing={1}>
						<Grid {...left}><Typography>Bezeichnung</Typography></Grid>
						<Grid {...right}><Typography>{asset.bezeichnung}</Typography></Grid>
						<Grid {...left}><Typography>Artikelnr.</Typography></Grid>
						<Grid {...right}><Typography>{asset.artikelId}</Typography></Grid>
						<Grid {...left}><Typography>Assetnr.</Typography></Grid>
						<Grid {...right}><Typography>{asset.assetnummer}</Typography></Grid>
						<Grid {...left}><Typography>Imei</Typography></Grid>
						<Grid {...right}><Typography>{asset.imei}</Typography></Grid>
						<Grid {...left}><Typography>Aktiv</Typography></Grid>
						<Grid {...right}><Typography>{asset.istAktiv}</Typography></Grid>
						<Grid {...left}><Typography>Seriennummer</Typography></Grid>
						<Grid {...right}><Typography>{asset.seriennummer}</Typography></Grid>
					</Grid>
				</StyledCard>
			</>
		);
	};

	const Auftragsdaten = () => {
		if (!props.dataItem) return;
		const serviceauftrag = props.dataItem.serviceauftrag.results.serviceauftragDetails;
		return (
			<>
				<StyledCard>
					<Grid container spacing={1}>
						<Grid item container xs={8} justify="center" alignItems="center">
							<Typography variant="h6">{serviceauftrag.name1}</Typography>
						</Grid>
						<Grid item xs={4} container justify="flex-end" alignItems="center">
							<TimerIcon/>
							<Typography variant="h6">{serviceauftrag.aufwandGeschaetzt}</Typography>
						</Grid>
					</Grid>
				</StyledCard>
				{
					serviceauftrag.positionen.map((position, key) => (
						<StyledCard key={key}>
							<Grid container spacing={1}>
								<Grid xs={12} item container justify="center"><Typography variant="h6" color="secondary">{position.menge}x {position.artikelbezeichnung}</Typography></Grid>
								<Grid xs={12} item container justify="center">
									<Typography variant="subtitle2">
										{position.langtext}
									</Typography>
								</Grid>
								<Grid {...left} item container justify="center">
									<Typography variant="subtitle2">
										Bestätigt:
									</Typography>
								</Grid>
								<Grid {...right} item container justify="center">
									<Typography variant="subtitle2">
										{+position.mengeBestaetigt}/{position.menge}
									</Typography>
								</Grid>
								<Grid {...left} item container justify="center">
									<Typography variant="subtitle2">
										Reserviert:
									</Typography>
								</Grid>
								<Grid {...right} item container justify="center">
									<Typography variant="subtitle2">
										{+position.mengeReserviert}/{position.menge}
									</Typography>
								</Grid>
								<Grid {...left} item container justify="center">
									<Typography variant="subtitle2">
										Eingebaut:
									</Typography>
								</Grid>
								<Grid {...right} item container justify="center">
									<Typography variant="subtitle2">
										{+position.mengeEingebaut}/{position.menge}
									</Typography>
								</Grid>
								<Grid {...left} item container justify="center">
									<Typography variant="subtitle2">
										Retourniert:
									</Typography>
								</Grid>
								<Grid {...right} item container justify="center">
									<Typography variant="subtitle2">
										{+position.mengeRetourniert}/{position.menge}
									</Typography>
								</Grid>
								<Grid {...left} item container justify="center">
									<Typography variant="subtitle2">
										Umgelagert:
									</Typography>
								</Grid>
								<Grid {...right} item container justify="center">
									<Typography variant="subtitle2">
										{+position.mengeUmgelagert}/{position.menge}
									</Typography>
								</Grid>
								<Grid {...left} item container justify="center">
									<Typography variant="subtitle2">
										Defekt eingelagert:
									</Typography>
								</Grid>
								<Grid {...right} item container justify="center">
									<Typography variant="subtitle2">
										{+position.mengeDefektEingelagert}/{position.menge}
									</Typography>
								</Grid>
							</Grid>
						</StyledCard>
					))
				}
			</>
		);
	};

	return [{
		ueberschrift: 'Kundendaten',
		Funktion: Kundendaten
	}, {
		ueberschrift: 'Assetdaten',
		Funktion: Assetdaten
	}, {
		ueberschrift: 'Auftragsdaten',
		Funktion: Auftragsdaten
	}];
};

export default detailViewServiceauftrag;
