import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import IconAvatar from './IconAvatar';
import NotificationsIcon from '@material-ui/icons/Notifications';
import IconWithText from './IconWithText';

const styles = theme => ({
	root: {
		width: '100%',
		marginBottom: '5px'
	}
});

const AccordionBelegkopf = props => {

	const { classes } = props;

	return (
		<div className={classes.root}>
			<ExpansionPanel expanded>

				<ExpansionPanelSummary>
					<IconWithText
						header={props.header}
						body={props.body}
					>
						<IconAvatar color="default"><NotificationsIcon className="icon" /></IconAvatar>
					</IconWithText>
				</ExpansionPanelSummary>

				<ExpansionPanelDetails>
					{props.children}
				</ExpansionPanelDetails>

			</ExpansionPanel>
		</div>
	);
};

AccordionBelegkopf.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AccordionBelegkopf);