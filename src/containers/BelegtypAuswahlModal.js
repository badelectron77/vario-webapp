import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import ReceiptIcon from '@material-ui/icons/Receipt';

const styles = theme => ({
	button: {
		marginBottom: theme.spacing(1)
	},
	icon: {
		marginRight: theme.spacing(1)
	}
});

const BelegtypAuswahlModal = props => {

	const { classes } = props;

	return (
		<Dialog
			open={props.open}
			onBackdropClick={props.closeBelegtypAuswahlModal}
		>
			<DialogTitle>Beleg übernehmen in:</DialogTitle>
			<DialogContent>
				<Button
					variant="contained"
					fullWidth
					color="secondary"
					className={classes.button}
					href={`/app/belegneu/${props.adresseId}/0100?belegId=${props.belegId}`}
				>
					<NoteAddIcon className={classes.icon} />
					Angebot
				</Button>
				<Button
					variant="contained"
					fullWidth
					color="secondary"
					href={`/app/belegneu/${props.adresseId}/0200?belegId=${props.belegId}`}
				>
					<ReceiptIcon className={classes.icon} />
					Auftrag
				</Button>
			</DialogContent>
			<DialogActions>
				<Button onClick={props.closeBelegtypAuswahlModal} color="primary">Abbrechen</Button>
			</DialogActions>
		</Dialog>
	);
}

export default withStyles(styles)(BelegtypAuswahlModal);
