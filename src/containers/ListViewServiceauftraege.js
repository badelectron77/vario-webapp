import React from "react";
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ImageIcon from '@material-ui/icons/Image';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Hidden from '@material-ui/core/Hidden';
import { withStyles } from '@material-ui/core';

const ListViewServiceauftraege = props => {

	const avatarStyles = theme => ({
		root: {
			backgroundColor: theme.palette.primary.main,
			marginRight: theme.spacing(1)
		}
	});
	const StyledAvatar = withStyles(avatarStyles)(Avatar);

	return (
		<List>
			{props.items ? props.items.map((serviceauftrag, i) => (
				<ListItem
					key={i}
					button
					divider={!!~(i - props.items.length)}
					selected={props.selectedId === serviceauftrag.id}
					onClick={() => props.writeUriAndSelectId(serviceauftrag.id)}
				>
					<StyledAvatar>
						<ImageIcon />
					</StyledAvatar>

					<ListItemText
						primary={`${serviceauftrag.name1}`}
						secondary={`${serviceauftrag.ort}`}
					/>

					<Hidden only={["xs", "md", "lg"]}>
						<ListItemSecondaryAction>
							<ListItemText primary={`${serviceauftrag.aufwandGeschaetzt} Std.`} />
						</ListItemSecondaryAction>
					</Hidden>
				</ListItem>
			)) : null}
		</List>
	);
};

export default ListViewServiceauftraege;
