import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
	root: {
		width: '100%',
		marginBottom: '10px'
	},
	heading: {
		fontSize: theme.typography.pxToRem(20)
	},
	secHeading: {
		fontSize: theme.typography.pxToRem(17),
		color: theme.palette.text.primary
	},
	thirdHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary
	}
});

const IconWithText = props => {

	const { classes } = props;

	const sb = props.subbody;
	let subbody;
	if (typeof sb === 'string') subbody = <Typography className={classes.thirdHeading}>{props.subbody}</Typography>;
	else subbody = sb;

	const b = props.body;
	let body;
	if (typeof b === 'string') body = <Typography className={classes.secHeading}>{props.body}</Typography>;
	else body = b;

	return (
		<div className={classes.root}>
			<Grid container wrap="nowrap" spacing={8}>
				<Grid item>
					{props.children}
				</Grid>
				<Grid item>
					<Typography className={classes.heading}>
						{props.header}
					</Typography>
					{body}
					{subbody}
				</Grid>
			</Grid>
		</div>
	);
}

IconWithText.propTypes = {
	classes: PropTypes.object.isRequired,
	header: PropTypes.string,
	body: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.object
	])
};

export default withStyles(styles)(IconWithText);