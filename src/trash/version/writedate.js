const fs = require('fs');

const options = {
	day: '2-digit',
	month: '2-digit',
	year: '2-digit'
};

const dateStr = new Date().toLocaleString('de-DE', options).replace(/-/g, '');

fs.writeFile('src/trash/version/date.txt', dateStr, err => {
	if (err) return console.warn(err);
});